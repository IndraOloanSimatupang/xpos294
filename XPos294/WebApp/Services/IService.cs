﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ViewModel;

namespace WebApp.Services
{
    public interface IService<T>
    {
        Task<List<T>> GetAll();

        Task<T> GetById(long id);

        Task<ResponseResult> Create(T ent);

        Task<ResponseResult> Update(T ent);

        Task<ResponseResult> Delete(T ent);
    }
}
