﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using ViewModel;
using WebApi.DataModels;
using WebApp.Security;

namespace WebApp.Services
{
    public class PerpustakaanService : IService<PerpustakaanViewModel>
    {
        private readonly IConfiguration _configuration;
        private readonly string webApiBaseUrl;
        private ResponseResult result = new ResponseResult();

        public PerpustakaanService(IConfiguration configuration)
        {
            _configuration = configuration;
            webApiBaseUrl = _configuration.GetValue<string>("WebApiBaseUrl");
        }

        public async Task<ResponseResult> Create(PerpustakaanViewModel ent)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                    string strPayload = JsonConvert.SerializeObject(ent);
                    HttpContent content = new StringContent(strPayload, Encoding.UTF8, "application/json");
                    using (var response = await httpClient.PostAsync(webApiBaseUrl + "/perpustakaan", content))
                    {
                        var apiResponse = await response.Content.ReadAsStringAsync();
                        result = JsonConvert.DeserializeObject<ResponseResult>(apiResponse);

                    }
                }
            }
            catch (Exception e)
            {

                result.Success = false;
                result.Message = e.Message;
                result.Entity = ent;
            }
            return result;
        }

        public async Task<ResponseResult> Delete(PerpustakaanViewModel ent)
        {
                using (var httpClient = new HttpClient())
                {
                   httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                    using (var response = await httpClient.DeleteAsync(webApiBaseUrl + $"/perpustakaan/{ent.Id}" ))
                    {
                        var apiResponse = await response.Content.ReadAsStringAsync();
                        result = JsonConvert.DeserializeObject<ResponseResult>(apiResponse);
                    }
                }
            return result;
        }

        public async Task<List<PerpustakaanViewModel>> GetAll()
        {
            List<PerpustakaanViewModel> result = new List<PerpustakaanViewModel>();
            using (var httpClient = new HttpClient())
            {
                //string token = ContextAccessor.GetToken();
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                using (var response = await httpClient.GetAsync(webApiBaseUrl + "/perpustakaan"))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<List<PerpustakaanViewModel>>(apiResponse);
                }
            }
            return result;
        }

        public async Task<PerpustakaanViewModel> GetById(long id)
        {
            PerpustakaanViewModel result = new PerpustakaanViewModel();
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                using (var response = await httpClient.GetAsync(webApiBaseUrl + $"/perpustakaan/{id}"))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<PerpustakaanViewModel>(apiResponse);
                }
            }
            return result;
        }

        public async Task<ResponseResult> Update(PerpustakaanViewModel ent)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ContextAccessor.GetToken());
                string strPayload = JsonConvert.SerializeObject(ent);
                HttpContent content = new StringContent(strPayload, Encoding.UTF8, "application/json");
                using (var response = await httpClient.PutAsync(webApiBaseUrl + "/perpustakaan", content))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<ResponseResult>(apiResponse);
                }
            }

            return result;
        }
    }
}
