﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ViewModel;
using WebApi.DataModels;
using WebApp.Services;

namespace WebApp.Controllers
{
    [Authorize(Roles = "Administrator, Perpustakaan")]
    public class PerpustakaanController : Controller
    {

        private readonly ILogger<PerpustakaanController> _logger;
        private readonly IConfiguration _configuration;
        private readonly string WebApiBaseUrl;

        private readonly PerpustakaanService perServ;

        public PerpustakaanController(ILogger<PerpustakaanController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            WebApiBaseUrl = _configuration.GetValue<string>("WebApiBaseUrl");
            perServ = new PerpustakaanService(configuration);
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> List()
        {
            List<PerpustakaanViewModel> list = await perServ.GetAll();
            return PartialView("_List", list);
        }

        public IActionResult Create()
        {
            return PartialView("_Create");
        }

        [HttpPost]
        public async Task<IActionResult> Create(PerpustakaanViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await perServ.Create(model);
                if (result.Success)
                {
                    ViewBag.title = "Create";
                    ViewBag.subtitle = "Created";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }

            }
            return PartialView("_Create", model);
        }

        public async Task<IActionResult> Edit(int id)
        {

            PerpustakaanViewModel result = await perServ.GetById(id);
            if (result != null)
            {
                return PartialView("_Edit", result);
            }
            return PartialView("_Edit", result);

        }


        [HttpPost]
        public async Task<IActionResult> Edit(PerpustakaanViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await perServ.Update(model);

                if (result.Success)
                {
                    ViewBag.title = "Edit";
                    ViewBag.subtitle = "Edited";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }


            }
            return PartialView("_Edit", model);
        }


        public async Task<IActionResult> Details(int id)
        {

            PerpustakaanViewModel result = await perServ.GetById(id);
          
                    if (result != null)
                    {
                        return PartialView("_Details", result);
                    }
            return PartialView("_Details", result);

        }

        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {

            PerpustakaanViewModel result = await perServ.GetById(id);
                    if (result != null)
                    {
                        return PartialView("_Delete", result);
                    }
            return PartialView("_Delete", result);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(PerpustakaanViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await perServ.Delete(model);
                if (result.Success)
                {
                    ViewBag.title = "Delete";
                    ViewBag.subtitle = "Deleted";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }

            }
            return PartialView("_Delete", model);
        }
    }
}
