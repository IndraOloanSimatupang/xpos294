﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ViewModel;
using WebApp.Services;

namespace WebApp.Controllers
{
    [Authorize(Roles = "Administrator, Category")]
    public class CategoryController : Controller
    {

        private readonly ILogger<CategoryController> _logger;
        private readonly IConfiguration _configuration;
        private readonly string WebApiBaseUrl;

        private readonly CategoryService catServ;

        public CategoryController(ILogger<CategoryController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            WebApiBaseUrl = _configuration.GetValue<string>("WebApiBaseUrl");
            catServ = new CategoryService(configuration);
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> List()
        {
            List<CategoryViewModel> list = await catServ.GetAll();
            return PartialView("_List", list);
        }

        public IActionResult Create()
        {
            return PartialView("_Create");
        }

        [HttpPost]
        public async Task<IActionResult> Create(CategoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await catServ.Create(model);
                if (result.Success)
                {
                    ViewBag.title = "Create";
                    ViewBag.subtitle = "Created";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }

            }
            return PartialView("_Create", model);
        }

        public async Task<IActionResult> Edit(int id)
        {

            CategoryViewModel result = await catServ.GetById(id);
            if (result != null)
            {
                return PartialView("_Edit", result);
            }
            return PartialView("_Edit", result);

        }


        [HttpPost]
        public async Task<IActionResult> Edit(CategoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await catServ.Update(model);

                if (result.Success)
                {
                    ViewBag.title = "Edit";
                    ViewBag.subtitle = "Edited";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }


            }
            return PartialView("_Edit", model);
        }


        public async Task<IActionResult> Details(int id)
        {

            CategoryViewModel result = await catServ.GetById(id);
          
                    if (result != null)
                    {
                        return PartialView("_Details", result);
                    }
            return PartialView("_Details", result);

        }

        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {

            CategoryViewModel result = await catServ.GetById(id);
                    if (result != null)
                    {
                        return PartialView("_Delete", result);
                    }
            return PartialView("_Delete", result);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(CategoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseResult result = await catServ.Delete(model);
                if (result.Success)
                {
                    ViewBag.title = "Delete";
                    ViewBag.subtitle = "Deleted";
                    return PartialView("_Success", model);
                }
                else
                {
                    ViewBag.ErrorMessage = result.Message;
                }

            }
            return PartialView("_Delete", model);
        }
    }
}
