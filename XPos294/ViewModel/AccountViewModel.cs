﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel
{
    public class UserLogin
    {
        [Required, Display(Name = "User Name")]
        public string UserName { get; set; }
        [Required, DataType(DataType.Password)]
        public string Password { get; set; }
    }
    public class AccountViewModel : UserLogin
    {
        public long Id { get; set; }

        [Required, MaxLength(50)]
        public string FirstName { get; set; }

        [Required, MaxLength(50)]
        public string LastName { get; set; }

        public string FullName 
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
        public bool Active { get; set; }

        public List<string> Roles { get; set; }

        public string Token { get; set; }
        public DateTime Expiration { get; set; }
    }
}
