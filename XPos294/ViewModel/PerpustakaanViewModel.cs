﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApi.DataModels
{
    public class PerpustakaanViewModel 
    {
       
        public long Id { get; set; }

        [Required, MaxLength(100)]
        public string NamaBuku { get; set; }

        [Required, MaxLength(50)]
        public string Penerbit { get; set; }

        [Required, MaxLength(50)]
        public string Pengarang { get; set; }


    }
}
