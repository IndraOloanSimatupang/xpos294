﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ViewModel
{
    public class UserRoleViewModel
    {
        public long Id { get; set; }

        public string UserName { get; set; }

        public string Role { get; set; }

        public bool Active { get; set; }
    }

}
