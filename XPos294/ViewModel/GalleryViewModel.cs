﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ViewModels
{
    public class GalleryViewModel
    {
      
        public long Id { get; set; }
        public IFormFile FilePath { get; set; }

        public string FileLink { get; set; }

        [Required, MaxLength(50)]
        public string Name { get; set; }

        [Required, MaxLength(500)]
        public string Description { get; set; }

        public bool Active { get; set; }
    }
}
