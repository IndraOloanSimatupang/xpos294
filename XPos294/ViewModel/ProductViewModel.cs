﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ViewModel
{
    public class ProductViewModel
    {
        public long Id { get; set; }
        [Display(Name = "Variant")]
        public long VariantId { get; set; }
        [Display(Name = "Variant")]
        public string VariantName { get; set; }

        [Required, MaxLength(10)]
        public string Initial { get; set; }

        [Required, MaxLength(50)]
        public string Name { get; set; }
        [Required, MaxLength(500)]
        public string Description { get; set; }
        public decimal Price { get; set; }
        public decimal Stock { get; set; }
        public bool Active { get; set; }
        public long CategoryId { get; internal set; }
    }
}
