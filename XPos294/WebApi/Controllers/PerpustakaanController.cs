﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using ViewModel;
using WebApi.DataModels;
using WebApi.Repositories;
using WebApi.Security;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ReadableBodyStream(Roles = "Administrator,Perpustakaan")]
    public class PerpustakaanController : ControllerBase
    {
        private PerpustakaanRepositories perpusRepo = new PerpustakaanRepositories(ClaimsContext.UserName());
        [HttpGet]
        public async Task<List<PerpustakaanViewModel>> Get()
        {
            return perpusRepo.GetAll();
        }

        [HttpGet("{Id}")]
        public async Task<PerpustakaanViewModel> Get(long Id)
        {
            return perpusRepo.GetById(Id);
        }

        [HttpPost]
        public async Task<ResponseResult> Post(PerpustakaanViewModel model)
        {
           
            return perpusRepo.Create(model);
        }

        [HttpPut]
        public async Task<ResponseResult> Put(PerpustakaanViewModel model)
        {
           
            return perpusRepo.Update(model);
        }

        [HttpDelete("{id}")]
        public async Task<ResponseResult> Delete(long id)
        {

            PerpustakaanViewModel model = new PerpustakaanViewModel() { Id = id };
            return perpusRepo.Delete(model);
        }
    }
}
