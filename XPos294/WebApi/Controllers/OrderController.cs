﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WebApi.Models;
using System.Collections.Generic;
using System.Linq;
using ViewModel;
using WebApi.Repositories;
using Microsoft.AspNetCore.Authorization;
using WebApi.Security;
using WebApi.DataModels;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ReadableBodyStream(Roles = "Administrator,Order")]
    public class OrderController : ControllerBase
    {
        private readonly OrderRepository orderRepo = new OrderRepository(ClaimsContext.UserName());


        //[HttpGet]
        //public async Task<List<OrderHeaderViewModel>> Get()
        //{
        //    return orderHeaderRepo.GetAll();
        //}

        //[HttpGet("{id}")]
        //public async Task<OrderHeaderViewModel> Get(long id)
        //{
        //    return orderHeaderRepo.GetById(id);
        //}

        [HttpPost]
        public async Task<ResponseResult> Post(OrderHeaderViewModel model)
        {
            //if (!orderRepo.Create(model).)
            //    return new OrderHeaderViewModel();
            return orderRepo.Create(model);
        }

        //[HttpPut]
        //public async Task<OrderHeaderViewModel> Put(OrderHeaderViewModel model)
        //{
        //    var result = orderHeaderRepo.Update(model);
        //    if (!result)
        //        return new OrderHeaderViewModel();
        //    return model;
        //}

        //[HttpDelete]
        //public async Task<OrderHeaderViewModel> Delete(OrderHeaderViewModel model)
        //{
        //    var result = orderHeaderRepo.Delete(model);
        //    if (!result)
        //        return new OrderHeaderViewModel();
        //    return model;
        //}


    }
}
