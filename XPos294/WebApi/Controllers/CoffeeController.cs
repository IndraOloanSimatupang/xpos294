﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Models;
namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CoffeeController : ControllerBase
    {
        [HttpGet]
        public async Task<Coffee> Get()
        {
            Coffee coffee = new Coffee();
            coffee.Type = "Robusta";
            coffee.Method = "V60";
            coffee.Ice = false;
            coffee.Colour = "Black";
            coffee.Caffeine = 60;
            coffee.Brand = "Gayo";

            return coffee;


        }
    }
}
