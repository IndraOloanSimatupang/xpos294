﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Threading.Tasks;
using ViewModel;
using ViewModels;
using WebApi.Repositories;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GalleryController : ControllerBase
    {
        private GalleryRepository garlleryRepo = new GalleryRepository();
        private string[] acceptedExt = new string[] { ".jpg", ".jpeg", ".png", ".gif" };

        [HttpPost]
        public async Task<ResponseResult> Post([FromForm] GalleryViewModel model)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                if (model.FilePath != null)
                {
                    string fileEXt = Path.GetExtension(model.FilePath.FileName);
                    if (Array.IndexOf(acceptedExt, fileEXt) != -1)
                    {
                        var uniqueFilename = GetUiniqueName(model.FilePath.FileName);
                        var uploads = Path.Combine("Resources", "Images");
                        var filePath = Path.Combine(uploads, uniqueFilename);

                        model.FilePath.CopyTo(new FileStream(filePath, FileMode.Create));
                        model.FileLink = uniqueFilename;

                        result = garlleryRepo.Create(model);
                    }
                }
                else
                {
                    result.Success = false;
                    result.Message = "File types accepted: " + String.Join(",", acceptedExt);
                }
            }
            catch (Exception e)
            {

                result.Message = e.Message;
                result.Success = false;
                result.Entity = StatusCode(500, $"Internal server error: {e}");
            }
            return result;
        }

        private string GetUiniqueName(string fileName)
        {
            fileName = Path.GetFileName(fileName);

            return Path.GetFileNameWithoutExtension(fileName)
                + "_" + Guid.NewGuid().ToString().Substring(0, 4)
                + Path.GetExtension(fileName);
        }
    }
}
