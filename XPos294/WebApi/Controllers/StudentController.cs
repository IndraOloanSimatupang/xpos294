﻿//using System;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using System.Threading.Tasks;
//using WebApi.Models;
//using System.Collections.Generic;
//using System.Linq;
//using ViewModel;
//using WebApi.Repositories;

//namespace WebApi.Controllers
//{
//    [Route("api/[controller]")]
//    [ApiController]
//    public class StudentController : ControllerBase
//    {
//        private StudentRepository studentRepo = new StudentRepository();

//        [HttpGet]
//        public async Task<List<StudentViewModel>> Get()
//        {
//            return studentRepo.GetAll();
//        }

//        [HttpGet("{id}")]
//        public async Task<StudentViewModel> Get(int id)
//        {
//            return studentRepo.GetById(id);
//        }

//        [HttpPost]
//        public async Task<StudentViewModel> Post(StudentViewModel model)
//        {
//            var result = studentRepo.Create(model);
//            if (!result)
//                return new StudentViewModel();
//            return model;
//        }

//        [HttpPut]
//        public async Task<StudentViewModel> Put(StudentViewModel model)
//        {
//            var result = studentRepo.Update(model);
//            if (!result)
//                return new StudentViewModel();
//            return model;
//        }

//        [HttpDelete]
//        public async Task<StudentViewModel> Delete(StudentViewModel model)
//        {
//            var result = studentRepo.Delete(model);
//            if (!result)
//                return new StudentViewModel();
//            return model;
//        }
//    }
//}

