﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using ViewModel;
using WebApi.Repositories;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Administrator,Variant")]
    public class VariantController : ControllerBase
    {
        private VariantRepository variantRepo = new VariantRepository();

        public async Task<List<VariantViewModel>> Get()
        {
            return variantRepo.GetAll();
        }

        [HttpGet("{Id}")]
        public async Task<VariantViewModel> Get(long Id)
        {
            return variantRepo.GetById(Id);
        }

        [HttpGet("category/{Id}")]
        public async Task<ResponseResult> ByParent(long Id)
        {
            return variantRepo.ByParent(Id);
        }
        [HttpGet("category2/{Id}")]
        public async Task<ResponseResult> ByParent2(long Id)
        {
            return variantRepo.ByParent2(Id);
        }

        [HttpPost]
        public async Task<ResponseResult> Post(VariantViewModel model)
        {
            //var result = variantRepo.Create(model);
            //if (!result)
            //    return new VariantViewModel();
            //return model;
            return variantRepo.Create(model);
        }

        [HttpPut]
        public async Task<ResponseResult> Put(VariantViewModel model)
        {
            //var result = variantRepo.Update(model);
            //if (!result)
            //    return new VariantViewModel();
            //return model;
            return variantRepo.Update(model);
        }

        [HttpDelete("{id}")]
        public async Task<ResponseResult> Delete(long id)
        {
            //var result = variantRepo.Delete(model);
            //if (!result)
            //    return new VariantViewModel();
            //return model;
            VariantViewModel model = new VariantViewModel() { Id = id };
            return variantRepo.Delete(model);
        }
    }
}
