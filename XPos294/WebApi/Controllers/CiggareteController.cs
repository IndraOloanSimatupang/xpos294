﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CiggareteController : ControllerBase
    {
        [HttpGet]
        public async Task<Ciggarete> Get()
        {
            Ciggarete ciggarete = new Ciggarete();
            ciggarete.Quantity = 20;
            ciggarete.Nicotine = 31;
            ciggarete.Tar = 2;
            ciggarete.Brand = "Marlboro";
            ciggarete.IncludeFilter = true;

            return ciggarete;
        

        }
    }
}
