﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApi.Migrations
{
    public partial class AddLibrary : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Perpustakaans",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifyBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifyDate = table.Column<DateTime>(nullable: true),
                    NamaBuku = table.Column<string>(maxLength: 100, nullable: false),
                    Penerbit = table.Column<string>(maxLength: 50, nullable: false),
                    Pengarang = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Perpustakaans", x => x.Id);
                });

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "admin",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 18, 9, 37, 32, 570, DateTimeKind.Local).AddTicks(6570));

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "spv1",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 18, 9, 37, 32, 570, DateTimeKind.Local).AddTicks(6656));

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "user1",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 18, 9, 37, 32, 570, DateTimeKind.Local).AddTicks(6651));

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "user2",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 18, 9, 37, 32, 570, DateTimeKind.Local).AddTicks(6654));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 18, 9, 37, 32, 567, DateTimeKind.Local).AddTicks(4292));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 18, 9, 37, 32, 568, DateTimeKind.Local).AddTicks(4770));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 18, 9, 37, 32, 568, DateTimeKind.Local).AddTicks(4805));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 18, 9, 37, 32, 570, DateTimeKind.Local).AddTicks(3200));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 18, 9, 37, 32, 570, DateTimeKind.Local).AddTicks(3523));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 18, 9, 37, 32, 570, DateTimeKind.Local).AddTicks(3537));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 18, 9, 37, 32, 570, DateTimeKind.Local).AddTicks(3540));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 18, 9, 37, 32, 569, DateTimeKind.Local).AddTicks(8550));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 18, 9, 37, 32, 569, DateTimeKind.Local).AddTicks(8652));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 18, 9, 37, 32, 569, DateTimeKind.Local).AddTicks(8655));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 18, 9, 37, 32, 569, DateTimeKind.Local).AddTicks(8656));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Perpustakaans");

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "admin",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 592, DateTimeKind.Local).AddTicks(6328));

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "spv1",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 592, DateTimeKind.Local).AddTicks(6397));

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "user1",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 592, DateTimeKind.Local).AddTicks(6392));

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "user2",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 592, DateTimeKind.Local).AddTicks(6395));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 589, DateTimeKind.Local).AddTicks(8352));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 590, DateTimeKind.Local).AddTicks(8184));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 590, DateTimeKind.Local).AddTicks(8223));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 592, DateTimeKind.Local).AddTicks(4123));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 592, DateTimeKind.Local).AddTicks(4223));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 592, DateTimeKind.Local).AddTicks(4227));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 592, DateTimeKind.Local).AddTicks(4230));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 592, DateTimeKind.Local).AddTicks(1366));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 592, DateTimeKind.Local).AddTicks(1502));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 592, DateTimeKind.Local).AddTicks(1505));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 592, DateTimeKind.Local).AddTicks(1507));
        }
    }
}
