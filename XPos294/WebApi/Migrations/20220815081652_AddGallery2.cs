﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApi.Migrations
{
    public partial class AddGallery2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "Galleries",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "Galleries",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "ModifyBy",
                table: "Galleries",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifyDate",
                table: "Galleries",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "admin",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 956, DateTimeKind.Local).AddTicks(2563));

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "spv1",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 956, DateTimeKind.Local).AddTicks(2634));

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "user1",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 956, DateTimeKind.Local).AddTicks(2628));

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "user2",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 956, DateTimeKind.Local).AddTicks(2631));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 953, DateTimeKind.Local).AddTicks(4504));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 954, DateTimeKind.Local).AddTicks(4546));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 954, DateTimeKind.Local).AddTicks(4585));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 956, DateTimeKind.Local).AddTicks(582));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 956, DateTimeKind.Local).AddTicks(683));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 956, DateTimeKind.Local).AddTicks(686));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 956, DateTimeKind.Local).AddTicks(688));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 955, DateTimeKind.Local).AddTicks(7870));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 955, DateTimeKind.Local).AddTicks(7955));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 955, DateTimeKind.Local).AddTicks(7958));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 955, DateTimeKind.Local).AddTicks(7960));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Galleries");

            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "Galleries");

            migrationBuilder.DropColumn(
                name: "ModifyBy",
                table: "Galleries");

            migrationBuilder.DropColumn(
                name: "ModifyDate",
                table: "Galleries");

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "admin",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 13, 33, 53, 145, DateTimeKind.Local).AddTicks(3198));

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "spv1",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 13, 33, 53, 145, DateTimeKind.Local).AddTicks(3401));

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "user1",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 13, 33, 53, 145, DateTimeKind.Local).AddTicks(3287));

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "user2",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 13, 33, 53, 145, DateTimeKind.Local).AddTicks(3290));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 13, 33, 53, 141, DateTimeKind.Local).AddTicks(8824));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 13, 33, 53, 143, DateTimeKind.Local).AddTicks(902));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 13, 33, 53, 143, DateTimeKind.Local).AddTicks(942));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 13, 33, 53, 145, DateTimeKind.Local).AddTicks(512));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 13, 33, 53, 145, DateTimeKind.Local).AddTicks(634));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 13, 33, 53, 145, DateTimeKind.Local).AddTicks(638));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 13, 33, 53, 145, DateTimeKind.Local).AddTicks(641));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 13, 33, 53, 144, DateTimeKind.Local).AddTicks(7087));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 13, 33, 53, 144, DateTimeKind.Local).AddTicks(7192));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 13, 33, 53, 144, DateTimeKind.Local).AddTicks(7195));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 13, 33, 53, 144, DateTimeKind.Local).AddTicks(7198));
        }
    }
}
