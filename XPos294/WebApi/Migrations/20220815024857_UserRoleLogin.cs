﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApi.Migrations
{
    public partial class UserRoleLogin : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_OrderHeaders_Referance",
                table: "OrderHeaders");

            migrationBuilder.DropColumn(
                name: "CreateBy",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "CreateDate",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "ModifyBy",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "ModifyDate",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "OrderHeaders");

            migrationBuilder.DropColumn(
                name: "Referance",
                table: "OrderHeaders");

            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "Categories");

            migrationBuilder.AddColumn<string>(
                name: "Reference",
                table: "OrderHeaders",
                maxLength: 15,
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "Accounts",
                columns: table => new
                {
                    UserName = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifyBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifyDate = table.Column<DateTime>(nullable: true),
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Password = table.Column<string>(maxLength: 200, nullable: false),
                    FirstName = table.Column<string>(maxLength: 50, nullable: false),
                    LastName = table.Column<string>(maxLength: 50, nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounts", x => x.UserName);
                });

            migrationBuilder.CreateTable(
                name: "GroupJobs",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Job = table.Column<string>(maxLength: 100, nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupJobs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TransRoles",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GroupJobId = table.Column<long>(nullable: false),
                    Role = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransRoles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TransRoles_GroupJobs_GroupJobId",
                        column: x => x.GroupJobId,
                        principalTable: "GroupJobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserRoles",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserName = table.Column<string>(maxLength: 50, nullable: false),
                    GroupJobId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserRoles_GroupJobs_GroupJobId",
                        column: x => x.GroupJobId,
                        principalTable: "GroupJobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserRoles_Accounts_UserName",
                        column: x => x.UserName,
                        principalTable: "Accounts",
                        principalColumn: "UserName",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Accounts",
                columns: new[] { "UserName", "Active", "CreatedBy", "CreatedDate", "FirstName", "Id", "LastName", "ModifyBy", "ModifyDate", "Password" },
                values: new object[,]
                {
                    { "admin", true, "Admin", new DateTime(2022, 8, 15, 9, 48, 57, 344, DateTimeKind.Local).AddTicks(4411), "Super", 1L, "User", null, null, "admin1234" },
                    { "user1", true, "Admin", new DateTime(2022, 8, 15, 9, 48, 57, 344, DateTimeKind.Local).AddTicks(4475), "User", 2L, "Satu", null, null, "user1234" },
                    { "user2", true, "Admin", new DateTime(2022, 8, 15, 9, 48, 57, 344, DateTimeKind.Local).AddTicks(4477), "User", 3L, "Dua", null, null, "user1234" },
                    { "spv1", true, "Admin", new DateTime(2022, 8, 15, 9, 48, 57, 344, DateTimeKind.Local).AddTicks(4479), "Spv", 4L, "Satu", null, null, "spv1234" }
                });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "Active", "CreatedBy", "CreatedDate", "Initial", "ModifyBy", "ModifyDate", "Name" },
                values: new object[,]
                {
                    { 1L, true, "Admin", new DateTime(2022, 8, 15, 9, 48, 57, 341, DateTimeKind.Local).AddTicks(6280), "MainCo", null, null, "Main Course" },
                    { 2L, true, "Admin", new DateTime(2022, 8, 15, 9, 48, 57, 342, DateTimeKind.Local).AddTicks(6151), "Drink", null, null, "Drink" },
                    { 3L, true, "Admin", new DateTime(2022, 8, 15, 9, 48, 57, 342, DateTimeKind.Local).AddTicks(6186), "Dessert", null, null, "Dessert" }
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Active", "CategoryId", "CreatedBy", "CreatedDate", "Description", "Initial", "ModifyBy", "ModifyDate", "Name", "Price", "Stock", "Variant Id", "VariantId" },
                values: new object[,]
                {
                    { 1L, true, 0L, "Admin", new DateTime(2022, 8, 15, 9, 48, 57, 344, DateTimeKind.Local).AddTicks(2447), "Capcay seafood", "NasCap", null, null, "Nasi Capcay", 25000m, 10m, null, 1L },
                    { 2L, true, 0L, "Admin", new DateTime(2022, 8, 15, 9, 48, 57, 344, DateTimeKind.Local).AddTicks(2546), "Dengan bumbu rempah", "AyKal", null, null, "Ayam Kalasan", 26000m, 10m, null, 3L },
                    { 3L, true, 0L, "Admin", new DateTime(2022, 8, 15, 9, 48, 57, 344, DateTimeKind.Local).AddTicks(2549), "Dengan gula aren", "KaMer", null, null, "Iced Kacang Merah", 18000m, 10m, null, 4L },
                    { 4L, true, 0L, "Admin", new DateTime(2022, 8, 15, 9, 48, 57, 344, DateTimeKind.Local).AddTicks(2552), "1/2 ekor", "AyGor", null, null, "Ayam Goreng", 18000m, 10m, null, 2L }
                });

            migrationBuilder.InsertData(
                table: "Variants",
                columns: new[] { "Id", "Active", "CategoryId", "CreatedBy", "CreatedDate", "Initial", "ModifyBy", "ModifyDate", "Name" },
                values: new object[,]
                {
                    { 1L, true, 1L, "Admin", new DateTime(2022, 8, 15, 9, 48, 57, 343, DateTimeKind.Local).AddTicks(9436), "Paket Nasi", null, null, "Paket Nasi" },
                    { 2L, true, 1L, "Admin", new DateTime(2022, 8, 15, 9, 48, 57, 343, DateTimeKind.Local).AddTicks(9528), "Ala Carte", null, null, "Ala Carte" },
                    { 3L, true, 1L, "Admin", new DateTime(2022, 8, 15, 9, 48, 57, 343, DateTimeKind.Local).AddTicks(9531), "Favorite", null, null, "Favorite" },
                    { 4L, true, 2L, "Admin", new DateTime(2022, 8, 15, 9, 48, 57, 343, DateTimeKind.Local).AddTicks(9533), "Iced", null, null, "Iced" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_OrderHeaders_Reference",
                table: "OrderHeaders",
                column: "Reference",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_GroupJobs_Job",
                table: "GroupJobs",
                column: "Job",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TransRoles_GroupJobId_Role",
                table: "TransRoles",
                columns: new[] { "GroupJobId", "Role" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserRoles_GroupJobId",
                table: "UserRoles",
                column: "GroupJobId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRoles_UserName_GroupJobId",
                table: "UserRoles",
                columns: new[] { "UserName", "GroupJobId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TransRoles");

            migrationBuilder.DropTable(
                name: "UserRoles");

            migrationBuilder.DropTable(
                name: "GroupJobs");

            migrationBuilder.DropTable(
                name: "Accounts");

            migrationBuilder.DropIndex(
                name: "IX_OrderHeaders_Reference",
                table: "OrderHeaders");

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.DropColumn(
                name: "Reference",
                table: "OrderHeaders");

            migrationBuilder.AddColumn<string>(
                name: "CreateBy",
                table: "Students",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreateDate",
                table: "Students",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "ModifyBy",
                table: "Students",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifyDate",
                table: "Students",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CategoryId",
                table: "OrderHeaders",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "Referance",
                table: "OrderHeaders",
                type: "nvarchar(15)",
                maxLength: 15,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<long>(
                name: "CategoryId",
                table: "Categories",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_OrderHeaders_Referance",
                table: "OrderHeaders",
                column: "Referance",
                unique: true);
        }
    }
}
