﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApi.Migrations
{
    public partial class AddvariantId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Variants_Variant Id",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_Variant Id",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "Variant Id",
                table: "Products");

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "admin",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 592, DateTimeKind.Local).AddTicks(6328));

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "spv1",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 592, DateTimeKind.Local).AddTicks(6397));

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "user1",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 592, DateTimeKind.Local).AddTicks(6392));

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "user2",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 592, DateTimeKind.Local).AddTicks(6395));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 589, DateTimeKind.Local).AddTicks(8352));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 590, DateTimeKind.Local).AddTicks(8184));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 590, DateTimeKind.Local).AddTicks(8223));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 592, DateTimeKind.Local).AddTicks(4123));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 592, DateTimeKind.Local).AddTicks(4223));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 592, DateTimeKind.Local).AddTicks(4227));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 592, DateTimeKind.Local).AddTicks(4230));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 592, DateTimeKind.Local).AddTicks(1366));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 592, DateTimeKind.Local).AddTicks(1502));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 592, DateTimeKind.Local).AddTicks(1505));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 16, 16, 23, 8, 592, DateTimeKind.Local).AddTicks(1507));

            migrationBuilder.CreateIndex(
                name: "IX_Products_VariantId",
                table: "Products",
                column: "VariantId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Variants_VariantId",
                table: "Products",
                column: "VariantId",
                principalTable: "Variants",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Variants_VariantId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_VariantId",
                table: "Products");

            migrationBuilder.AddColumn<long>(
                name: "Variant Id",
                table: "Products",
                type: "bigint",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "admin",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 956, DateTimeKind.Local).AddTicks(2563));

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "spv1",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 956, DateTimeKind.Local).AddTicks(2634));

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "user1",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 956, DateTimeKind.Local).AddTicks(2628));

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "user2",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 956, DateTimeKind.Local).AddTicks(2631));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 953, DateTimeKind.Local).AddTicks(4504));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 954, DateTimeKind.Local).AddTicks(4546));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 954, DateTimeKind.Local).AddTicks(4585));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 956, DateTimeKind.Local).AddTicks(582));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 956, DateTimeKind.Local).AddTicks(683));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 956, DateTimeKind.Local).AddTicks(686));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 956, DateTimeKind.Local).AddTicks(688));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 955, DateTimeKind.Local).AddTicks(7870));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 955, DateTimeKind.Local).AddTicks(7955));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 955, DateTimeKind.Local).AddTicks(7958));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 15, 16, 51, 955, DateTimeKind.Local).AddTicks(7960));

            migrationBuilder.CreateIndex(
                name: "IX_Products_Variant Id",
                table: "Products",
                column: "Variant Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Variants_Variant Id",
                table: "Products",
                column: "Variant Id",
                principalTable: "Variants",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
