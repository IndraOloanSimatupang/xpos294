﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApi.Migrations
{
    public partial class RoleData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "admin",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 50, 1, 184, DateTimeKind.Local).AddTicks(257));

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "spv1",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 50, 1, 184, DateTimeKind.Local).AddTicks(326));

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "user1",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 50, 1, 184, DateTimeKind.Local).AddTicks(321));

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "user2",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 50, 1, 184, DateTimeKind.Local).AddTicks(324));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 50, 1, 180, DateTimeKind.Local).AddTicks(5522));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 50, 1, 181, DateTimeKind.Local).AddTicks(8892));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 50, 1, 181, DateTimeKind.Local).AddTicks(8940));

            migrationBuilder.InsertData(
                table: "GroupJobs",
                columns: new[] { "Id", "Active", "Job" },
                values: new object[,]
                {
                    { 1L, true, "Administrator" },
                    { 4L, true, "Cashier" },
                    { 3L, true, "Staff" },
                    { 2L, true, "Supervisor" }
                });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 50, 1, 183, DateTimeKind.Local).AddTicks(8189));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 50, 1, 183, DateTimeKind.Local).AddTicks(8286));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 50, 1, 183, DateTimeKind.Local).AddTicks(8289));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 50, 1, 183, DateTimeKind.Local).AddTicks(8292));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 50, 1, 183, DateTimeKind.Local).AddTicks(5417));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 50, 1, 183, DateTimeKind.Local).AddTicks(5511));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 50, 1, 183, DateTimeKind.Local).AddTicks(5514));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 50, 1, 183, DateTimeKind.Local).AddTicks(5516));

            migrationBuilder.InsertData(
                table: "TransRoles",
                columns: new[] { "Id", "GroupJobId", "Role" },
                values: new object[,]
                {
                    { 1L, 2L, "Category" },
                    { 2L, 2L, "Variant" },
                    { 3L, 2L, "Product" },
                    { 4L, 2L, "Order" },
                    { 5L, 3L, "Category" },
                    { 6L, 3L, "Variant" },
                    { 7L, 3L, "Product" },
                    { 8L, 4L, "Order" }
                });

            migrationBuilder.InsertData(
                table: "UserRoles",
                columns: new[] { "Id", "GroupJobId", "UserName" },
                values: new object[,]
                {
                    { 1L, 1L, "admin" },
                    { 2L, 2L, "spv1" },
                    { 3L, 3L, "user1" },
                    { 4L, 4L, "user2" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "TransRoles",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "TransRoles",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "TransRoles",
                keyColumn: "Id",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "TransRoles",
                keyColumn: "Id",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                table: "TransRoles",
                keyColumn: "Id",
                keyValue: 5L);

            migrationBuilder.DeleteData(
                table: "TransRoles",
                keyColumn: "Id",
                keyValue: 6L);

            migrationBuilder.DeleteData(
                table: "TransRoles",
                keyColumn: "Id",
                keyValue: 7L);

            migrationBuilder.DeleteData(
                table: "TransRoles",
                keyColumn: "Id",
                keyValue: 8L);

            migrationBuilder.DeleteData(
                table: "UserRoles",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "UserRoles",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "UserRoles",
                keyColumn: "Id",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "UserRoles",
                keyColumn: "Id",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                table: "GroupJobs",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "GroupJobs",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "GroupJobs",
                keyColumn: "Id",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "GroupJobs",
                keyColumn: "Id",
                keyValue: 4L);

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "admin",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 48, 57, 344, DateTimeKind.Local).AddTicks(4411));

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "spv1",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 48, 57, 344, DateTimeKind.Local).AddTicks(4479));

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "user1",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 48, 57, 344, DateTimeKind.Local).AddTicks(4475));

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "UserName",
                keyValue: "user2",
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 48, 57, 344, DateTimeKind.Local).AddTicks(4477));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 48, 57, 341, DateTimeKind.Local).AddTicks(6280));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 48, 57, 342, DateTimeKind.Local).AddTicks(6151));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 48, 57, 342, DateTimeKind.Local).AddTicks(6186));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 48, 57, 344, DateTimeKind.Local).AddTicks(2447));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 48, 57, 344, DateTimeKind.Local).AddTicks(2546));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 48, 57, 344, DateTimeKind.Local).AddTicks(2549));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 48, 57, 344, DateTimeKind.Local).AddTicks(2552));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 48, 57, 343, DateTimeKind.Local).AddTicks(9436));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 48, 57, 343, DateTimeKind.Local).AddTicks(9528));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 48, 57, 343, DateTimeKind.Local).AddTicks(9531));

            migrationBuilder.UpdateData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 15, 9, 48, 57, 343, DateTimeKind.Local).AddTicks(9533));
        }
    }
}
