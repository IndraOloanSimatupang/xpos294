﻿//using System;
//using System.Collections.Generic;
//using ViewModel;
//using System.Linq;
//using WebApi.DataModels;

//namespace WebApi.Repositories
//{
//    public class UserRoleRepository : IRepository<UserRoleViewModel>
//    {
//        private XPosDbContext _XPosDbContext = new XPosDbContext();

//        private string _UserName;

//        public UserRoleRepository()
//        {
//            _UserName = "Indra";
//        }

//        public UserRoleRepository(string username)
//        {
//            _UserName = username;
//        }

//        public bool Create(UserRoleViewModel entity)
//        {
//            bool result = true;
//            try
//            {
//                UserRole item = new UserRole();
//                item.UserName = entity.UserName;
//                item.Role = entity.Role;
//                item.Active = entity.Active;

//                item.CreatedBy = _UserName;
//                item.CreatedDate = DateTime.Now;

//                _XPosDbContext.UserRoles.Add(item);
//                _XPosDbContext.SaveChanges();
//            }
//            catch (Exception e)
//            {
//                string error = e.Message;
//            }
//            return result;
//        }

//        public bool Delete(UserRoleViewModel entity)
//        {
//            bool result = true;
//            try
//            {
//                UserRole item = _XPosDbContext.UserRoles
//                            .Where(o => o.Id == entity.Id)
//                            .FirstOrDefault();
//                if (item != null)
//                {
//                    _XPosDbContext.UserRoles.Remove(item);
//                    _XPosDbContext.SaveChanges();
//                }
//                else
//                {
//                    result = false;
//                }
//            }
//            catch (Exception e)
//            {
//                string error = e.Message;
//            }
//            return result;
//        }

//        public List<UserRoleViewModel> GetAll()
//        {
//            List<UserRoleViewModel> result = new List<UserRoleViewModel>();
//            try
//            {
//                result = (from o in _XPosDbContext.UserRoles
//                          select new UserRoleViewModel
//                          {
//                              Id = o.Id,
//                              UserName = o.UserName,
//                              Role = o.Role,
//                              Active = o.Active
//                          }).ToList();
//            }
//            catch (Exception e)
//            {
//                string error = e.Message;
//            }
//            return result;
//        }

//        public UserRoleViewModel GetById(long id)
//        {
//            UserRoleViewModel result = new UserRoleViewModel();
//            try
//            {
//                result = (from o in _XPosDbContext.UserRoles
//                          where o.Id == id
//                          select new UserRoleViewModel
//                          {
//                              Id = o.Id,
//                              UserName = o.UserName,
//                              Role = o.Role,
//                              Active = o.Active
//                          }).FirstOrDefault();
//            }
//            catch (Exception e)
//            {
//                string error = e.Message;
//            }
//            return result;
//        }

//        public bool Update(UserRoleViewModel entity)
//        {
//            bool result = true;
//            try
//            {
//                UserRole item = _XPosDbContext.UserRoles
//                            .Where(o => o.Id == entity.Id)
//                            .FirstOrDefault();
//                if (item != null)
//                {
//                    item.UserName = entity.UserName;
//                    item.Role = entity.Role;
//                    item.Active = entity.Active;

//                    item.ModifyBy = _UserName;
//                    item.ModifyDate = DateTime.Now;

//                    _XPosDbContext.SaveChanges();
//                }
//                else
//                {
//                    result = false;
//                }
//            }
//            catch (Exception e)
//            {
//                string error = e.Message;
//            }
//            return result;
//        }
//    }
//}

