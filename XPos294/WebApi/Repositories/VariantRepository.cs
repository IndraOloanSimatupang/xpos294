﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using ViewModel;
using WebApi.DataModels;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Repositories
{
    public class VariantRepository : IRepository<VariantViewModel>
    {
        private XPosDbContext _XPosDbContext = new XPosDbContext();

        private ResponseResult result = new ResponseResult();

        private string _UserName;

        public VariantRepository()
        {
            _UserName = "Indra";
        }
        public VariantRepository(string userName)
        {
            _UserName = userName;
        }

        public ResponseResult ByParent(long id)
        {
            try
            {
                result.Entity = (from o in _XPosDbContext.Variants
                                 where o.CategoryId == id
                                  select new VariantViewModel
                                  {
                                      Id = o.Id,
                                      CategoryId = o.CategoryId,
                                      CategoryName = o.Category.Name,
                                      Initial = o.Initial,
                                      Name = o.Name,
                                      Active = o.Active
                                  }).ToList();
            }
            catch (Exception e)
            {

                result.Success = false;
                result.Message = e.Message;
            }
            return result;
        }       
        
        public ResponseResult ByParent2(long id)
        {
            try
            {
                result.Entity = _XPosDbContext.Variants
                                 .Where (o => o.CategoryId == id)
                                 .Include("Category")
                                 .ToList();
            }
            catch (Exception e)
            {

                result.Success = false;
                result.Message = e.Message;
            }
            return result;
        }

        public ResponseResult Create(VariantViewModel entity)
        {
            
            try
            {
                Variant item = new Variant();
                item.CategoryId = entity.CategoryId;
                item.Initial = entity.Initial;
                item.Name = entity.Name;
                item.Active = entity.Active;

                item.CreatedBy = _UserName;
                item.CreatedDate = DateTime.Now;

                _XPosDbContext.Variants.Add(item);
                _XPosDbContext.SaveChanges();

                result.Entity = item;
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;

            }
            return result;
        }


        public ResponseResult Delete(VariantViewModel entity)
        {
            
            try
            {
                Variant item = _XPosDbContext.Variants
                    .Where(o => o.Id == entity.Id)
                    .FirstOrDefault();
                if (item != null)
                {
                    result.Entity = item;
                    _XPosDbContext.Variants.Remove(item);
                    _XPosDbContext.SaveChanges();
                }
                else
                {
                    result.Success = false;
                    result.Message = "Not Found";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {

                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public List<VariantViewModel> GetAll()
        {
            List<VariantViewModel> result = new List<VariantViewModel>();
            try
            {
                result = (from o in _XPosDbContext.Variants
                          select new VariantViewModel
                          {
                              Id = o.Id,
                              CategoryId = o.CategoryId,
                              CategoryName = o.Category.Name,
                              Initial = o.Initial,
                              Name = o.Name,
                              Active = o.Active
                          }).ToList();
            }
            catch (Exception e)
            {

                string error = e.Message;
            }
            return result;
        }

        public VariantViewModel GetById(long id)
        {
            VariantViewModel result = new VariantViewModel();
            try
            {
                result = (from o in _XPosDbContext.Variants
                          where o.Id == id
                          select new VariantViewModel
                          {
                              Id = o.Id,
                              CategoryId = o.CategoryId,
                              CategoryName= o.Category.Name,
                              Initial = o.Initial,
                              Name = o.Name,
                              Active = o.Active
                          }).FirstOrDefault();
            }
            catch (Exception e)
            {

                string error = e.Message;
            }
            return result;
        }


        public ResponseResult Update(VariantViewModel entity)
        {
            try
            {
                Variant item = _XPosDbContext.Variants
                  .Where(o => o.Id == entity.Id)
                   .FirstOrDefault();
                if (item != null)
                {
                    item.CategoryId = entity.CategoryId;
                    item.Initial = entity.Initial;
                    item.Name = entity.Name;
                    item.Active = entity.Active;

                    item.ModifyBy = _UserName;
                    item.ModifyDate = DateTime.Now;

                    _XPosDbContext.SaveChanges();
                    result.Entity = item;
                }
                else
                {
                    result.Success = false;
                    result.Message = "Not Found!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {

                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }
    }
}
