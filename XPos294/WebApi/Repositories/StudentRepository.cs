﻿//using System;
//using System.Collections.Generic;
//using ViewModel;
//using System.Linq;
//using WebApi.DataModels;
//using WebApi.Models;
//using Student = WebApi.DataModels.Student;

//namespace WebApi.Repositories
//{
//    public class StudentRepository : IRepository<StudentViewModel>
//    {
//        private readonly XPosDbContext _XPosDbContext = new XPosDbContext();

//        private string _UserName;

//        public StudentRepository()
//        {
//            _UserName = "Indra";
//        }

//        public StudentRepository(string username)
//        {
//            _UserName = username;
//        }

//        public bool Create(StudentViewModel entity)
//        {
//            bool result = true;
//            try
//            {
//                Student item = new Student();
//                item.FirstName = entity.FirstName;
//                item.LastName = entity.LastName;
//                item.BirthDate = entity.BirthDate;
//                item.Gender = entity.Gender;


//                _XPosDbContext.Students.Add(item);
//                _XPosDbContext.SaveChanges();
//            }
//            catch (Exception e)
//            {
//                string error = e.Message;
//            }
//            return result;
//        }

//        public bool Delete(StudentViewModel entity)
//        {
//            bool result = true;
//            try
//            {
//                Student item = _XPosDbContext.Students
//                           .Where(x => x.Id == entity.Id)
//                           .FirstOrDefault();
//                if (item != null)
//                {
//                    _XPosDbContext.Students.Remove(item);
//                    _XPosDbContext.SaveChanges();
//                }
//                else
//                {
//                    result = false;
//                }
//            }
//            catch (Exception e)
//            {
//                string error = e.Message;
//            }
//            return result;
//        }

//        public List<StudentViewModel> GetAll()
//        {
//            List<StudentViewModel> result = new List<StudentViewModel>();
//            try
//            {
//                result = (from o in _XPosDbContext.Students
//                          select new StudentViewModel
//                          {
//                              Id = o.Id,
//                              FirstName = o.FirstName,
//                              LastName = o.LastName,
//                              BirthDate = o.BirthDate,
//                              Gender = o.Gender
//                          }).ToList();
//            }
//            catch (Exception e)
//            {
//                string error = e.Message;
//            }
//            return result;
//        }

//        public StudentViewModel GetById(long id)
//        {
//            StudentViewModel result = new StudentViewModel();
//            try
//            {
//                result = (from o in _XPosDbContext.Students
//                          where o.Id == id
//                          select new StudentViewModel
//                          {
//                              Id = o.Id,
//                              FirstName = o.FirstName,
//                              LastName = o.LastName,
//                              BirthDate = o.BirthDate,
//                              Gender = o.Gender
//                          }).FirstOrDefault();
//            }
//            catch (Exception e)
//            {
//                string error = e.Message;
//            }
//            return result;
//        }

//        public bool Update(StudentViewModel entity)
//        {
//            bool result = true;
//            try
//            {
//                DataModels.Student item = _XPosDbContext.Students
//                           .Where(o => o.Id == entity.Id)
//                           .FirstOrDefault();
//                if (item != null)
//                {
//                    item.FirstName = entity.FirstName;
//                    item.LastName = entity.LastName;
//                    item.BirthDate = entity.BirthDate;
//                    item.Gender = entity.Gender;



//                    _XPosDbContext.SaveChanges();
//                }
//                else
//                {
//                    result = false;
//                }
//            }
//            catch (Exception e)
//            {
//                string error = e.Message;
//            }
//            return result;
//        }
//    }
//}

