﻿//using System;
//using System.Collections.Generic;
//using ViewModel;
//using System.Linq;
//using WebApi.DataModels;

//namespace WebApi.Repositories
//{
//    public class FileCollectionRepository : IRepository<FileCollectionViewModel>
//    {
//        private XPosDbContext _XPosDbContext = new XPosDbContext();

//        private string _UserName;

//        public FileCollectionRepository()
//        {
//            _UserName = "Indra";
//        }

//        public FileCollectionRepository(string username)
//        {
//            _UserName = username;
//        }

//        public bool Create(FileCollectionViewModel entity)
//        {
//            bool result = true;
//            try
//            {
//                FileCollection item = new FileCollection();
//                item.Title = entity.Title;
//                item.FileName = entity.FileName;
//                item.Active = entity.Active;

//                item.CreatedBy = _UserName;
//                item.CreatedDate = DateTime.Now;

//                _XPosDbContext.FileCollections.Add(item);
//                _XPosDbContext.SaveChanges();
//            }
//            catch (Exception e)
//            {
//                string error = e.Message;
//            }
//            return result;
//        }

//        public bool Delete(FileCollectionViewModel entity)
//        {
//            bool result = true;
//            try
//            {
//                FileCollection item = _XPosDbContext.FileCollections
//                            .Where(o => o.Id == entity.Id)
//                            .FirstOrDefault();
//                if (item != null)
//                {
//                    _XPosDbContext.FileCollections.Remove(item);
//                    _XPosDbContext.SaveChanges();
//                }
//                else
//                {
//                    result = false;
//                }
//            }
//            catch (Exception e)
//            {
//                string error = e.Message;
//            }
//            return result;
//        }

//        public List<FileCollectionViewModel> GetAll()
//        {
//            List<FileCollectionViewModel> result = new List<FileCollectionViewModel>();
//            try
//            {
//                result = (from o in _XPosDbContext.FileCollections
//                          select new FileCollectionViewModel
//                          {
//                              Id = o.Id,
//                              Title = o.Title,
//                              FileName = o.FileName,
//                              Active = o.Active
//                          }).ToList();
//            }
//            catch (Exception e)
//            {
//                string error = e.Message;
//            }
//            return result;
//        }

//        public FileCollectionViewModel GetById(long id)
//        {
//            FileCollectionViewModel result = new FileCollectionViewModel();
//            try
//            {
//                result = (from o in _XPosDbContext.FileCollections
//                          where o.Id == id
//                          select new FileCollectionViewModel
//                          {
//                              Id = o.Id,
//                              Title = o.Title,
//                              FileName = o.FileName,
//                              Active = o.Active
//                          }).FirstOrDefault();
//            }
//            catch (Exception e)
//            {
//                string error = e.Message;
//            }
//            return result;
//        }

//        public bool Update(FileCollectionViewModel entity)
//        {
//            bool result = true;
//            try
//            {
//                FileCollection item = _XPosDbContext.FileCollections
//                            .Where(o => o.Id == entity.Id)
//                            .FirstOrDefault();
//                if (item != null)
//                {
//                    item.Title = entity.Title;
//                    item.FileName = entity.FileName;
//                    item.Active = entity.Active;

//                    item.ModifyBy = _UserName;
//                    item.ModifyDate = DateTime.Now;

//                    _XPosDbContext.SaveChanges();
//                }
//                else
//                {
//                    result = false;
//                }
//            }
//            catch (Exception e)
//            {
//                string error = e.Message;
//            }
//            return result;
//        }
//    }
//}
