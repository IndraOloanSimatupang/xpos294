﻿using System;
using System.Collections.Generic;
using ViewModel;
using System.Linq;
using WebApi.DataModels;

namespace WebApi.Repositories
{
    public class AccountRepository
    {
        private XPosDbContext _XPosDbContext = new XPosDbContext();

        public AccountViewModel Authentication(UserLogin userLogin)
        {
            AccountViewModel result = new AccountViewModel();
            result = _XPosDbContext.Accounts
                .Where(o => o.UserName == userLogin.UserName && o.Password == userLogin.Password)
                .Select(o => new AccountViewModel
                {
                    //Password Jangan Dimunculkan
                    Id = o.Id,
                    UserName = o.UserName,
                    FirstName = o.FirstName,
                    LastName = o.LastName,
                    Active = o.Active
                }).FirstOrDefault();

            if (result != null)
            {
                result.Roles = userLogin.UserName == "admin" ? new List<string> { "Administrator" } : Authorization(userLogin.UserName);
            }

            return result;
        }

        public List<string> Authorization(string userName)
        {
            List<string> result = new List<string>();
            var List = (from us in _XPosDbContext.Accounts
                        join ur in _XPosDbContext.UserRoles
                            on us.UserName equals ur.UserName into JoinUserRole

                        from usUr in JoinUserRole.DefaultIfEmpty()
                        join gj in _XPosDbContext.GroupJobs
                            on usUr.GroupJobId equals gj.Id into JoinUserRoleGroup

                        from URG in JoinUserRoleGroup.DefaultIfEmpty()
                        join tr in _XPosDbContext.TransRoles
                            on URG.Id equals tr.GroupJobId into JoinURGTrans

                        from urgt in JoinURGTrans.DefaultIfEmpty()
                        where us.UserName == userName
                        select new { urgt.Role }
                        ).ToList();
            //_XPosDbContext.UserRoles
            //.Where(o => o.UserName == userName)
            //.ToList();

            foreach (var item in List)
                result.Add(item.Role);

            return result;
        }

    }
}

