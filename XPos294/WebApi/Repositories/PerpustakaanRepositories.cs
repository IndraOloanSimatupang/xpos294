﻿using System;
using System.Collections.Generic;
using System.Linq;
using ViewModel;
using WebApi.DataModels;

namespace WebApi.Repositories
{
    public class PerpustakaanRepositories : IRepository<PerpustakaanViewModel>
    {

        private XPosDbContext _XPosDbContext = new XPosDbContext();

        private ResponseResult result = new ResponseResult();

        private string _UserName;

        public PerpustakaanRepositories()
        {
            _UserName = "Indra";
        }
        public PerpustakaanRepositories(string userName)
        {
            _UserName = userName;
        }

        public ResponseResult ByParent(long id)
        {
            throw new System.NotImplementedException();
        }

        public ResponseResult Create(PerpustakaanViewModel entity)
        {
            try
            {
                Perpustakaan item = new Perpustakaan();
                item.Id = entity.Id;
                item.NamaBuku = entity.NamaBuku;
                item.Pengarang = entity.Pengarang;
                item.Penerbit = entity.Penerbit;

                item.CreatedBy = _UserName;
                item.CreatedDate = DateTime.Now;

                _XPosDbContext.Perpustakaans.Add(item);
                _XPosDbContext.SaveChanges();

                result.Entity = item;

            }
            catch (Exception e)
            {

                result.Entity = entity;
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public ResponseResult Delete(PerpustakaanViewModel entity)
        {
            try
            {
                Perpustakaan item = _XPosDbContext.Perpustakaans
                    .Where(o => o.Id == entity.Id)
                    .FirstOrDefault();
                if (item != null)
                {
                    result.Entity = item;
                    _XPosDbContext.Perpustakaans.Remove(item);
                    _XPosDbContext.SaveChanges();
                }
                else
                {
                    result.Success = false;
                    result.Message = "Not Found";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {

                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public List<PerpustakaanViewModel> GetAll()
        {
            List<PerpustakaanViewModel> result = new List<PerpustakaanViewModel>();
            try
            {
                result = (from o in _XPosDbContext.Perpustakaans
                          select new PerpustakaanViewModel
                          {
                              Id = o.Id,
                              NamaBuku = o.NamaBuku,
                              Pengarang = o.Pengarang,
                              Penerbit = o.Penerbit
                              
                             
                          }).ToList();
            }
            catch (Exception e)
            {

                string error = e.Message;
            }
            return result;
        }

        public PerpustakaanViewModel GetById(long id)
        {
            PerpustakaanViewModel result = new PerpustakaanViewModel();
            try
            {
                result = (from o in _XPosDbContext.Perpustakaans
                          where o.Id == id
                          select new PerpustakaanViewModel
                          {
                              Id = o.Id,
                              NamaBuku = o.NamaBuku,
                              Pengarang = o.Pengarang,
                              Penerbit = o.Penerbit
                          }).FirstOrDefault();
            }
            catch (Exception e)
            {

                string error = e.Message;
            }
            return result;
        }

        public ResponseResult Update(PerpustakaanViewModel entity)
        {
            try
            {
                Perpustakaan item = _XPosDbContext.Perpustakaans
                    .Where(o => o.Id == entity.Id)
                     .FirstOrDefault();
                if (item != null)
                {
                    item.NamaBuku = entity.NamaBuku;
                    item.Penerbit = entity.Penerbit;
                    item.Pengarang = entity.Pengarang;

                    item.ModifyBy = _UserName;
                    item.ModifyDate = DateTime.Now;

                    _XPosDbContext.SaveChanges();
                    result.Entity = item;
                }
                else
                {
                    result.Success = false;
                    result.Message = "Not Found!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {

                result.Success = false;
                result.Message = e.Message;
            }
            return result;
        }
    }

}
