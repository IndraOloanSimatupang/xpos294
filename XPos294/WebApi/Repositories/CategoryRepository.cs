﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using ViewModel;
using WebApi.DataModels;

namespace WebApi.Repositories
{
    public class CategoryRepository : IRepository<CategoryViewModel>
    {
        private XPosDbContext _XPosDbContext = new XPosDbContext();

        private ResponseResult result = new ResponseResult();

        private string _UserName;

        public CategoryRepository()
        {
            _UserName = "Indra";
        }
        public CategoryRepository(string userName)
        {
            _UserName = userName;
        }

        public ResponseResult ByParent(long id)
        {
            throw new NotImplementedException();
        }

        public ResponseResult Create(CategoryViewModel entity)
        {
           
            try
            {
                Category item = new Category();
                item.Initial = entity.Initial;
                item.Name = entity.Name;
                item.Active = entity.Active;

                item.CreatedBy = _UserName;
                item.CreatedDate = DateTime.Now;

                _XPosDbContext.Categories.Add(item);
                _XPosDbContext.SaveChanges();

                result.Entity = item;
            }
            catch (Exception e)
            {
                result.Entity = entity;
                result.Message = e.Message;
                result.Success = false;
                
            }
            return result;
        }

        public ResponseResult Delete(CategoryViewModel entity)
        {
           
            try
            {
                Category item = _XPosDbContext.Categories
                    .Where(o => o.Id == entity.Id)
                    .FirstOrDefault();
                if (item != null)
                {
                    result.Entity = item;
                    _XPosDbContext.Categories.Remove(item);
                    _XPosDbContext.SaveChanges();
                }else
                {
                    result.Success = false;
                    result.Message = "Not Found";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {

                result.Message = e.Message;
                result.Success = false;
            }
            return result;  
        }

        public List<CategoryViewModel> GetAll()
        {
            List<CategoryViewModel> result = new List<CategoryViewModel>();
            try
            {
                result =(from o in _XPosDbContext.Categories
                         select new CategoryViewModel
                         {
                             Id = o.Id,
                             Initial = o.Initial,
                             Name = o.Name,
                             Active = o.Active
                         }).ToList();
            }
            catch (Exception e)
            {

                string error = e.Message;
            }
            return result;
        }

        public CategoryViewModel GetById(long id)
        {
            CategoryViewModel result = new CategoryViewModel();
            try
            {
                result = (from o in _XPosDbContext.Categories
                          where o.Id == id
                          select new CategoryViewModel
                          {
                              Id = o.Id,
                              Initial = o.Initial,
                              Name = o.Name,
                              Active = o.Active
                          }).FirstOrDefault();
            }
            catch (Exception e)
            {

                string error = e.Message;
            }
            return result;
        }

        public ResponseResult Update(CategoryViewModel entity)
        {
            
            try
            {
                Category item = _XPosDbContext.Categories
                    .Where(o => o.Id == entity.Id)
                     .FirstOrDefault();
                if (item != null)
                {
                    item.Initial = entity.Initial;
                    item.Name = entity.Name;
                    item.Active = entity.Active;

                    item.ModifyBy = _UserName;
                    item.ModifyDate = DateTime.Now;

                    _XPosDbContext.SaveChanges();
                    result.Entity = item;
                }else
                {
                    result.Success = false;
                    result.Message = "Not Found!";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {

                result.Success = false;
                result.Message = e.Message;
            }
            return result;
        }
    }
}
