﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using ViewModel;
using WebApi.Models;

namespace WebApi.Repositories
{
    public interface IRepository<T>
    {
        List<T> GetAll();
        T GetById(long id);
        ResponseResult ByParent(long id);
        ResponseResult Create(T entity);
        ResponseResult Update(T entity);
        ResponseResult Delete(T entity);
    }
}
