﻿using System;
using System.Linq;
using System.Collections.Generic;
using ViewModel;
using ViewModels;
using WebApi.DataModels;

namespace WebApi.Repositories
{
    public class GalleryRepository : IRepository<GalleryViewModel>
    {
        private readonly XPosDbContext _XPosDbContext = new XPosDbContext();
        private ResponseResult result = new ResponseResult();

        public ResponseResult ByParent(long id)
        {
            throw new NotImplementedException();
        }

        public ResponseResult Create(GalleryViewModel entity)
        {
            try
            {
                Gallery item = new Gallery();
                item.FileLink = entity.FileLink;
                item.Name = entity.Name;
                item.Description = entity.Description;
                item.Active = entity.Active;

                item.CreatedBy = "Admin";
                item.CreatedDate = DateTime.Now;

                _XPosDbContext.Galleries.Add(item);
                _XPosDbContext.SaveChanges();

                result.Entity = item;
            }
            catch (Exception e)
            {

                result.Message = e.Message;
                result.Success = false;
                result.Entity = entity;
            }
            return result;
        }

        public ResponseResult Delete(GalleryViewModel entity)
        {
            throw new System.NotImplementedException();
        }

        public List<GalleryViewModel> GetAll()
        {
            List<GalleryViewModel> result = new List<GalleryViewModel>();
            try
            {
                result = (from o in _XPosDbContext.Galleries
                          select new GalleryViewModel
                          {
                              Id = o.Id,
                              FileLink = o.FileLink,
                              Name = o.Name,
                              Description = o.Description,
                              Active = o.Active
                          }).ToList();
            }
            catch (Exception e)
            {

                string s = e.Message;
            }
            return result;
        }

        public GalleryViewModel GetById(long id)
        {
            GalleryViewModel result = new GalleryViewModel();
            try
            {
                result = (from o in _XPosDbContext.Galleries
                          where o.Id == id
                          select new GalleryViewModel
                          {
                              Id = o.Id,
                              FileLink = o.FileLink,
                              Name = o.Name,
                              Description = o.Description,
                              Active = o.Active
                          }).FirstOrDefault();
            }
            catch (Exception e)
            {

                string s = e.Message;
            }
            return result;
        }

        public ResponseResult Update(GalleryViewModel entity)
        {
            throw new System.NotImplementedException();
        }
    }
}
