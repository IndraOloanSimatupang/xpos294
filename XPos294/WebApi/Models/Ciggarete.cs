﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models
{
    public class Ciggarete
    {
        public int Quantity { get; set; }
        public int Nicotine { get; set; }
        public int Tar { get; set; }
        public string Brand { get; set; }
        public bool IncludeFilter { get; set; }
    }
}
