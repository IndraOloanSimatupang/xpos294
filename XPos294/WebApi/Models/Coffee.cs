﻿namespace WebApi.Models
{
    public class Coffee
    {
        public string Type { get; set; }
        public string Method { get; set; }
        public bool Ice { get; set; }
        public string Colour { get; set; }
        public int  Caffeine { get; set; }
        public string Brand { get; set; }
    }
}
