﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Models;


namespace WebApi.DataModels
{
    public class Student
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required, MaxLength(10)]
        public string FirstName { get; set; }
        [Required, MaxLength(10)]
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        [Required, MaxLength(6)]
        public string Gender { get; set; }
    }
}
